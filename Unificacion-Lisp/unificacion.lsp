(setf lista '(/ (g x y) z))
 (setf lista2 '((/ A X) (/ b y) (/ c w) (/ d z)))
 (setf a '(P A (? X)))
 (setf b '(P (? Y) B))
 (setf X '(P (? X) (? X)))
 (setf Y '(P A B))

(defun whatIs(var)
    (cond ((atom var) T)
        ;no es átomo, luego es lista
        ((/= 2 (length var) ) 'L) ;si no es ni átomo ni variable retorna L
        ((not (eq (first var)'?)) 'L) ;Si tiene longitud 2 pero no empiza por una ?
        ((eq (first var) '?) 'V) ;devuelve V si es variable
        (T NIL)
    )
)

(defun isAtom (var)
    (cond ((atom var) T)
          ((/= 2 (length var) ) NIL) ;es una lista, luego no es átomo
          ((eq (first var) '?) T) ;es variable, luego es átomo
          (T NIL)
    )
)



(defun APLICAR (lista aplicaciones)
    ;(print "APLICAR")
    ;(print lista)
    ;(print aplicaciones)
    (loop
        (cond ((not (setf aux (pop aplicaciones))) (return lista)) ;si aplicaciones vacia retorna
              (T (setf lista(subst (second aux) (third aux) lista :test #'equal)))
        )
    )
)

(defun MEMBERRECURSIVO (lista miembro)
  (cond ((not (consp lista)) NIL)
        ((member miembro lista) T)
        ((equal (whatIs (first lista)) 'L) (MEMBERRECURSIVO (first lista) miembro))
        (T (MEMBERRECURSIVO (rest lista) miembro))
  )
)




(defun COMPOSICION (lista componer)
  ;(print "COMPOSICION")
  ;(print lista)
  ;(print componer)
    (cond ((equalp NIL lista) (first componer)) 
          (T (setf compuesta (list '/ (APLICAR (second lista) componer) (third lista)))
             (loop
                 ;(print "DEBUG")
                 ;(print compuesta)
                 ;(print componer)
                 (cond ((not (setf aux (pop componer))) (return compuesta))
                   ((not(MEMBERRECURSIVO compuesta (third aux))) (setf compuesta (list compuesta aux)))
                   (T NIL)
                 )
             )
          )
    )
)



(defun AnyAtomSwap(E1 E2) ;No funciona porque creo que E1 y E2 son locales, y no se ve el resultado en el exterior.
       (cond  ((isAtom E1) (list E2 E1))
              ((isAtom E2) (list E2 E1)) ;retorna la lista con los elementos swapeados.
              (T NIL)))



(defun Swap(E1 E2)
  (rotatef E1 E2))


(defun UNIFICAR (E1 E2)
    ;(print "E1 y E2")
    ;(print E1)
    ;(print E2)
    (prog (F1 T1 F2 T2 Z1 G1 G2 Z2 aux)
        
        (when (setf aux (AnyAtomSwap E1 E2))
            (setf E1 (pop aux))
            (setf E2 (pop aux))
            (cond   ((equalp E1 E2) (return NIL))
                    ((equalp 'V (whatIs E1))
                     (if (MEMBERRECURSIVO E2 E1) (return 'FALLO) (return (list '/ E2 E1))))

                    ((eq 'V (whatIs E2)) (return (list '/ E1 E2)))
                    (T (return 'FALLO))
            )
        )
        
        (setf F1 (first E1))
        (setf T1 (rest E1))
        (setf F2 (first E2))
        (setf T2 (rest E2))
        
        (setf Z1 (UNIFICAR F1 F2))
        (when (equalp Z1 'FALLO) (return 'FALLO))
        
        (setf G1 (APLICAR T1 (list Z1)))
        (setf G2 (APLICAR T2 (list Z1)))
        (setf Z2 (UNIFICAR G1 G2))
        (if (equalp Z2 'FALLO) (return 'FALLO) (return (COMPOSICION Z1 (list Z2))))
    )
)
