/*****************************************************************************

		Copyright (c) My Company

 Project:  ALMACEN
 FileName: ALMACEN.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "almacen.inc"

domains
  posicion=symbol
  id=symbol
  objeto=symbol
  distancia=integer
  unidades=integer
  lineapedido=linea(objeto,unidades)
  pedido=lineapedido*
  est=estado(posicion, objeto)
  lista=est*
  
predicates

  estanteria(id,posicion)
  conectado(posicion,posicion,distancia)
  ubicacion(objeto,id,unidades)
  calcularDestino(lineapedido, posicion)
  resuelve(pedido, lista, distancia, posicion)
  solucion(pedido, posicion)
  posiblesDestinos(est, posicion, distancia)
  escribe(lista)
  miembro(est, lista)

clauses

  /* Contenido del almacen */
  ubicacion(patatas,s1,200).
  ubicacion(melones,s1,100).
  ubicacion(boligrafos,s2,500).
  ubicacion(boligrafos,s3,400).
  ubicacion(melocotones,s4,200).
  ubicacion(berzas,s4,100).
  ubicacion(papeles,s5,500).
  ubicacion(boligrafos,s6,400).
  ubicacion(plumas,s7,500).
  ubicacion(plumas,s8,400).
  ubicacion(colonias,s3,150).
  ubicacion(ratones,s4,210).
  
  /* Estanterķas */
  estanteria(s1,ps1).
  estanteria(s2,ps2).
  estanteria(s3,ps3).
  estanteria(s4,ps4).
  estanteria(s5,ps5).
  estanteria(s6,ps6).
  estanteria(s7,ps7).
  estanteria(s8,ps8).
  
  /*Conexiones pasillos y estanterķas */
  conectado(pS, pc1, 10).
  conectado(pc1, pc2, 10).
  conectado(pc2, pc3, 10).
  conectado(pc3, pc4, 10).
  conectado(pc4, pT, 10).
  conectado(ps1, pc1, 5).
  conectado(ps5, pc1, 5).
  conectado(ps2, pc2, 5).
  conectado(ps6, pc2, 5).
  conectado(ps3, pc3, 5).
  conectado(ps7, pc3, 5).
  conectado(ps4, pc4, 5).
  conectado(ps8, pc4, 5).
  
  
  resuelve([], Estados, DistanciaAcumulada, Destino):-
  	Estados = [estado(Actual, _)|_],
  	Actual = pT,
  	Destino = pT,
  	escribe(Estados),
  	write("Distancia Total = ",DistanciaAcumulada,'\n').
  
  resuelve([], Estados, DistanciaAcumulada, Destino):-
  	Estados = [H|_],
  	H = estado(Actual, _),
  	Destino = Actual,
  	posiblesDestinos(H, Siguiente, Distancia),
  	not(miembro(estado(Siguiente, salir), Estados)),
  	NuevoEstados = [estado(Siguiente, salir)|Estados],
  	NuevaDistancia = DistanciaAcumulada + Distancia,
  	resuelve([], NuevoEstados, NuevaDistancia, pT).
  
  resuelve(Pedido, Estados, DistanciaAcumulada, Destino):-
  	Estados = [H|_],
  	H = estado(Actual, _),
  	Destino = Actual,
  	Pedido = [SiguientePedido|RestoPedidos],
  	calcularDestino(SiguientePedido, NuevoDestino),
  	posiblesDestinos(H, Siguiente, Distancia),
  	SiguientePedido = linea(Objeto, _),
  	not(miembro(estado(Siguiente, Objeto), Estados)),
  	NuevoEstados = [estado(Siguiente, Objeto)|Estados],
  	NuevaDistancia = DistanciaAcumulada + Distancia,
  	resuelve(RestoPedidos, NuevoEstados, NuevaDistancia, NuevoDestino).
  
  
  resuelve(Pedido, Estados, DistanciaAcumulada, Destino):-
  	Estados = [H|_],
  	posiblesDestinos(H, Siguiente, Distancia),
  	H = estado(_, Objeto),
  	not(miembro(estado(Siguiente, Objeto), Estados)),
  	NuevoEstados = [estado(Siguiente, Objeto)|Estados],
  	NuevaDistancia = DistanciaAcumulada + Distancia,
  	resuelve(Pedido, NuevoEstados, NuevaDistancia, Destino).
  	
   calcularDestino(linea(Obj, Unidades), PosObjetivo):-
	ubicacion(Obj, Estanteria, Total),
	Total >= Unidades,
	estanteria(Estanteria, PosObjetivo).
   
   posiblesDestinos(estado(Actual, _), Siguiente, Distancia):-
  	conectado(Actual, Siguiente, Distancia);
  	conectado(Siguiente, Actual, Distancia).
  
  /*Estados repetidos */
  miembro(E,[E|_]).
  miembro(E,[_|T]):-
  	miembro(E,T).
  	
  /*Escritura de la lista, excepto el primer elemento */
  escribe([]).
  escribe([H|T]):-
  	escribe(T),
        write(H,'\n').
  	
  solucion(Pedido, Inicio):-
  	Pedido = [linea(Objeto,_)|_],
  	resuelve(Pedido,[estado(Inicio, Objeto)], 0 ,Inicio).
          
          
  

goal
        solucion([linea(patatas,40),linea(boligrafos,40),linea(plumas,10)], ps).











